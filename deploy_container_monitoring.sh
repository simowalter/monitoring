#!/bin/bash

docker run -d --name node-exporter -p 9100:9100 quay.io/prometheus/node-exporter
docker run -d --name prometheus -p 9090:9090 -v ./prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
docker run -d --name grafana -p 3005:3000 grafana/grafana:8.4.3-arm64
docker run -d -p 9080:8080 \
  -v=/:/rootfs:ro \
  -v=/var/run:/var/run:ro \
  -v=/sys:/sys:ro \
  -v=/var/lib/docker/:/var/lib/docker:ro \
  -v=/dev/disk/:/dev/disk:ro \
  --privileged \
  --device=/dev/kmsg \
  --name=cadvisor \
  spcodes/cadvisor:linux-arm64